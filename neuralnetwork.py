def encode_labels(labels, max_index):
    """Encode the labels into binary vectors."""
    # Allocate the output labels, all zeros.
    encoded = np.zeros((labels.shape[0], max_index + 1))
    
    # Fill in the ones at the right indices.
    for i in range(labels.shape[0]):
        encoded[i, labels[i]] = 1
    return encoded

def accuracy(predicted, actual):
    total = 0.0
    correct = 0.0
    for p, a in zip(predicted, actual):
        total += 1
        if p == a:
            correct += 1
    return correct / total
    
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
import theano.tensor as T
from theano import *

mat_dict = scipy.io.loadmat('/Users/jithon/Desktop/common/data.mat')

test_Y = mat_dict['test_labels']
test_X = mat_dict['test_images'].T
train_Y = mat_dict['train_labels']
train_X = mat_dict['train_images'].T

mean_X = np.mean(train_X,axis=0)
std_X = np.std(train_X,axis=0)
nonzero_index = std_X!=0
# test_X = (test_X[:,nonzero_index]-mean_X[nonzero_index])/std_X[nonzero_index]
# train_X = (train_X[:,nonzero_index]-mean_X[nonzero_index])/std_X[nonzero_index]

# Initialize shared weight variables
W1_shape = (50, test_X.shape[1])
b1_shape = 50
W2_shape = (10, 50)
b2_shape = 10

W1 = shared(np.random.random(W1_shape) - 0.5)
b1 = shared(np.random.random(b1_shape) - 0.5)
W2 = shared(np.random.random(W2_shape) - 0.5)
b2 = shared(np.random.random(b2_shape) - 0.5)

# Symbolic inputs
x = T.dmatrix('x') # N x 784
labels = T.dmatrix('labels') # N x 10

# Symbolic outputs
hidden = T.nnet.sigmoid(x.dot(W1.transpose()) + b1)
output = T.nnet.softmax(hidden.dot(W2.transpose()) + b2)
prediction = T.argmax(output, axis=1)
reg_lambda = 0.0001
regularization = reg_lambda * ((W1 * W1).sum() + (W2 * W2).sum() + (b1 * b1).sum() + (b2 * b2).sum())
cost = T.nnet.binary_crossentropy(output, labels).mean() + regularization

# Output functions
compute_prediction = function([x], prediction)

# Training functions
alpha = T.dscalar('alpha')
weights = [W1, W2, b1, b2]
updates = [(w, w - alpha * grad(cost, w)) for w in weights]
train_nn = function([x, labels, alpha],cost,updates=updates)

alpha = 10.0
labeled = encode_labels(train_Y, 9)

costs = []
while True:
    costs.append(float(train_nn(train_X, labeled, alpha)))

    if len(costs) % 10 == 0:
        print('Epoch', len(costs), 'with cost', costs[-1], 'and alpha', alpha)
    if len(costs) > 2 and costs[-2] - costs[-1] < 0.0001:
        if alpha < 0.2:
            break
        else:
            alpha = alpha / 1.5
                 
prediction = compute_prediction(test_X)
print(accuracy(prediction, test_Y))

plt.plot(range(len(costs)), costs)
plt.title('Cost vs training epoch')


