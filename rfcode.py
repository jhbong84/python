from time import time
from operator import itemgetter
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.preprocessing import Imputer
from collections import Counter
from sklearn.ensemble import RandomForestClassifier
from scipy.stats import randint as sp_randint
from sklearn.grid_search import GridSearchCV, RandomizedSearchCV
from sklearn.metrics import log_loss, make_scorer
from sklearn import linear_model
from unbalanced_dataset.over_sampling import OverSampler
from sklearn.utils import resample
from sklearn import cross_validation

# def linreg_score(train_X,train_Y):
    #regr = linear_model.LinearRegression()

    # Train the model using the training sets
    #regr.fit(train_X, train_Y)

    # Explained variance score: 1 is perfect prediction
    #print('Variance score: %.2f' % regr.score(test_X, test_Y))


def report(grid_scores, n_top=10):
    top_scores = sorted(grid_scores, key=itemgetter(1), reverse=True)[:n_top]
    for i, score in enumerate(top_scores):
        print("Model with rank: {0}".format(i + 1))
        print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
              score.mean_validation_score,
              np.std(score.cv_validation_scores)))
        print("Parameters: {0}".format(score.parameters))
        print("")
        
def log_loss_score(A1,test_data):
    
    i=0
    CEL = np.zeros([40,])
    
    for exp in np.arange(-5,-1,0.1):
        
        threshold = 1-10**exp
        
        A1[np.where(A1[:,0]>threshold),0] = threshold
        A1[np.where(A1[:,0]==threshold),1] = 1-threshold

        A1[np.where(A1[:,1]>threshold),1] = threshold
        A1[np.where(A1[:,1]==threshold),0] = 1-threshold
        
        # A1[np.where(test_data['EMPLOYEE_GROUP']>=1),1]=1
        # A1[np.where(test_data['EMPLOYEE_GROUP']>=1),0]=0
    
        CEL[i] = -(np.sum(np.log10(A1[np.where(test_data['RESIGNED']==0),0])) + np.sum(np.log10(A1[np.where(test_data['RESIGNED']==1),1])))/A1.shape[0]
        i +=1
        
    plt.figure()
    plt.plot(np.arange(-5,-1,0.1),CEL)
    plt.show()
    return CEL

def showdata(data):
    
    # data['na_count'] = np.sum(pd.isnull(data),axis=1)
    # data = data.dropna()
    data= data.drop(['RESIGNED'],1)
    num_null_data = np.sum(pd.isnull(data),axis=0)

    
    important_features = ['DIVORCE_REMARRIED_WITHIN_2_YEARS', 'NATIONALITY',
       'DIVORCE_WITHIN_2_YEARS', 'MOVE_HOUSE_T_2',
       'MARRIED_WITHIN_2_YEARS', 'COUNTRY_OF_BIRTH', 'UPGRADED_LAST_3_YRS',
       'HOUSING_GROUP', 'UPGRADED_CERT_3_YRS', 'SVC_INJURY_TYPE',
       'UNIT_CHG_LAST_3_YRS', 'HOUSE_UPG_DGRD', 'MARITAL_STATUS',
       'PREV_HOUSING_TYPE', 'UPGRADED_CERT_DESC_3_YRS', 'PROMO_LAST_2_YRS',
       'UNIT_CHG_LAST_1_YR', 'HSP_ESTABLISHMENT', 'NO_OF_KIDS',
       'HSP_CERTIFICATE', 'UNIT_CHG_LAST_2_YRS', 'HOUSING_TYPE',
       'IPPT_SCORE', 'PES_SCORE', 'SERVICE_SUB_AREA', 'HSP_CERT_RANK',
       'PROMO_LAST_4_YRS', 'HOUSING_RANK', 'PARENT_SERVICE',
       'PROMO_LAST_3_YRS', 'HSP_CERT_DESC', 'MIN_CHILD_AGE',
       'HOMETOWORKDIST', 'PROMO_LAST_5_YRS', 'AVE_CHILD_AGE',
       'PROMO_LAST_1_YR', 'UNIT', 'GENDER', 'VOC', 'RANK_GROUPING',
       'AWARDS_RECEIVED', 'YEARS_IN_GRADE', 'RANK_GRADE', 'AGE',
       'SERVICE_TYPE', 'YEARS_OF_SERVICE', 'TOT_PERC_INC_LAST_1_YR',
       'BAS_PERC_INC_LAST_1_YR', 'EMPLOYEE_GROUP']

    
    return num_null_data

def processdata(train,test):
    # from sklearn.preprocessing import OneHotEncoder
    
    if sum(train.columns=='STATUS'):
        train = train.drop(['RESIGN_DATE', 'RESIGNATION_MTH', 'RESIGNATION_QTR', 'RESIGNATION_YEAR','STATUS'],1)

    train = train.drop(['PERID','AGE_GROUPING'],1)
    train = train.replace('UNKNOWN',np.nan)    
    
    test = test.drop(['PERID','AGE_GROUPING'],1)
    test = test.replace('UNKNOWN',np.nan) 
    
    train.NO_OF_KIDS[train.MARITAL_STATUS=='Single']=0
    test.NO_OF_KIDS[test.MARITAL_STATUS=='Single']=0
    
    train.MIN_CHILD_AGE[train.MARITAL_STATUS=='Single']=0
    test.MIN_CHILD_AGE[test.MARITAL_STATUS=='Single']=0
    
    train.AVE_CHILD_AGE[train.MARITAL_STATUS=='Single']=0
    test.AVE_CHILD_AGE[test.MARITAL_STATUS=='Single']=0

    num_null_train = showdata(train)

    drop_cols = ['DIVORCE_REMARRIED_WITHIN_2_YEARS', 'NATIONALITY', 'DIVORCE_WITHIN_2_YEARS', 'MOVE_HOUSE_T_2', 
    'MARRIED_WITHIN_2_YEARS', 'COUNTRY_OF_BIRTH', 'UPGRADED_LAST_3_YRS', 'HOUSING_GROUP', 'UPGRADED_CERT_3_YRS', 
    'SVC_INJURY_TYPE', 'UNIT_CHG_LAST_3_YRS', 'HOUSE_UPG_DGRD', 'MARITAL_STATUS', 'PREV_HOUSING_TYPE',
    'UPGRADED_CERT_DESC_3_YRS','IPPT_SCORE','PROMO_LAST_2_YRS','PES_SCORE','HOUSING_TYPE','HOUSING_RANK',
    'UNIT_CHG_LAST_1_YR','UNIT_CHG_LAST_2_YRS']# 'EMPLOYEE_GROUP'
    train = train.drop(drop_cols,1)
    test = test.drop(drop_cols,1)
    
    # train['na_count'] = np.sum(pd.isnull(train),axis=1)
    # test['na_count'] = np.sum(pd.isnull(test),axis=1)
    
    # train['na_count'] = pd.isnull(train.BAS_PERC_INC_LAST_1_YR).astype(int)
    # test['na_count'] = pd.isnull(test.BAS_PERC_INC_LAST_1_YR).astype(int)
    num_null_train = showdata(train)
    
    graded_col = ['AGE','YEARS_IN_GRADE','YEARS_OF_SERVICE','NO_OF_KIDS','MIN_CHILD_AGE','AVE_CHILD_AGE','HSP_CERT_RANK',
    'PROMO_LAST_5_YRS','PROMO_LAST_4_YRS','PROMO_LAST_3_YRS','PROMO_LAST_2_YRS','PROMO_LAST_1_YR','AWARDS_RECEIVED',
    'HOUSING_RANK','HOUSE_UPG_DGRD','IPPT_SCORE','PES_SCORE','HOMETOWORKDIST','SVC_INJURY_TYPE','TOT_PERC_INC_LAST_1_YR',
    'BAS_PERC_INC_LAST_1_YR'] # ,'na_count'
    
    graded_col = set.difference(set(graded_col),set(drop_cols))
            
    categories_col = set.difference(set(train.columns),set.union(set(graded_col),['RESIGNED']))
    
    for col in categories_col:
            
        if col == 'HSP_ESTABLISHMENT':
            mapvalue['Primary'] = 0
            mapvalue['Secondary'] = 1
            mapvalue['Diploma'] = 2
            mapvalue['Technical Education'] = 3
            mapvalue['Pre University'] = 4
            mapvalue['University'] = 5
            
        else:
            data_counter = Counter(train[col])+Counter(test[col])
            data_counter_mostcommon = data_counter.most_common()
            i=0
            mapvalue = {}
            for item in data_counter_mostcommon:
                if isinstance(item[0],str):
                    mapvalue[item[0]] = i
                    i+=1    
            
        train = train.replace({col:mapvalue})
        test = test.replace({col:mapvalue})
        
    # train = train.ix[train.EMPLOYEE_GROUP==0,:]
    # test = test.ix[test.EMPLOYEE_GROUP==0,:]   
        # oversampling resigned = 1 group
        # downsampling resigned = 0 group
   #  newRESIGNED = pd.DataFrame(resample(train[train.RESIGNED==1]))
    #newRESIGNED1 = pd.DataFrame(resample(train[train.RESIGNED==1]))
    #newRESIGNED2 = pd.DataFrame(resample(train[train.RESIGNED==1]))
    #newRESIGNED3 = pd.DataFrame(resample(train[train.RESIGNED==1]))
    
    #train = train.append(newRESIGNED,ignore_index=True).append(newRESIGNED1,ignore_index=True).append(newRESIGNED2,ignore_index=True)
    
    # multiple imputations
    for col in categories_col:    
        enc = Imputer(strategy='most_frequent')   
        enc.fit(train[col].reshape([train[col].shape[0],1]))
        new_traincol = enc.transform(train[col].reshape([train[col].shape[0],1]))
        new_testcol = enc.transform(test[col].reshape([test[col].shape[0],1]))
        train[col] = new_traincol
        test[col] = new_testcol
        # train[col] = train[col].astype('float32')
        # test[col] = test[col].astype('float32')
        
    for col in graded_col:        
        enc = Imputer(strategy='median')   
        enc.fit(train[col].reshape([train[col].shape[0],1]))
        new_traincol = enc.transform(train[col].reshape([train[col].shape[0],1]))
        new_testcol = enc.transform(test[col].reshape([test[col].shape[0],1]))
        train[col] = new_traincol
        test[col] = new_testcol
        # train[col] = train[col].astype('float32')
        # test[col] = test[col].astype('float32')
        
    train['bonus']=train.TOT_PERC_INC_LAST_1_YR-train.BAS_PERC_INC_LAST_1_YR
    test['bonus']=test.TOT_PERC_INC_LAST_1_YR-test.BAS_PERC_INC_LAST_1_YR
    train['year_enter_army']=train.AGE-train.YEARS_OF_SERVICE
    test['year_enter_army']=test.AGE-test.YEARS_OF_SERVICE
    train['year_enter_grade']=train.AGE-train.YEARS_IN_GRADE
    test['year_enter_grade']=test.AGE-test.YEARS_IN_GRADE
    train['awards/service']=train.AWARDS_RECEIVED/train.YEARS_OF_SERVICE
    test['awards/service']=test.AWARDS_RECEIVED/test.YEARS_OF_SERVICE

    return train,test

# import xgboost as xgb

training = 0

print("Load the training/test data using pandas")
train = pd.read_csv("/Users/jithon/Desktop/mindef/20150803115609-HR_Retention_2013_training.csv")
test  = pd.read_csv("/Users/jithon/Desktop/mindef/20150803115608-HR_Retention_2013_to_be_predicted.csv")

if training == 1:
    old_train_shape = train.shape[0]
    perm_index = np.random.permutation(old_train_shape)
    #train_data = train.iloc[perm_index[0:10000],:]
    #test_data = train.iloc[perm_index[10000:old_train_shape],:]
    train_data = train
    test_data = test
else:
    train_data = train
    test_data = test

[train_data,test_data] = processdata(train_data,test_data)

#plt.figure()
#plt.hist(np.array(train_data.EMPLOYEE_GROUP[train.RESIGNED==1]))
#plt.figure()
#plt.hist(np.array(train_data.EMPLOYEE_GROUP[train.RESIGNED==0]))

features = list(set.difference(set(train_data.columns),['RESIGNED']))

print("Train a Random Forest model")
rf = RandomForestClassifier(max_features= 13,n_estimators=2000, n_jobs=-1,class_weight= "auto",criterion='entropy',bootstrap=False)
    # log_loss1 = make_scorer(log_loss, eps=0.001)
    # scores = cross_validation.cross_val_score(rf,train_data[features], train_data['RESIGNED'], scoring=log_loss1)
rf.fit(train_data[features], train_data['RESIGNED'])
print('Done the fitting')

A = rf.predict(test_data[features])
A1 = rf.predict_proba(test_data[features])

if training != 1:

    threshold = 0.999
        
    A1[np.where(A1[:,0]>threshold),0] = threshold
    A1[np.where(A1[:,0]==threshold),1] = 1-threshold

    A1[np.where(A1[:,1]>threshold),1] = threshold
    A1[np.where(A1[:,1]==threshold),0] = 1-threshold

    # A1[np.where(test_data['EMPLOYEE_GROUP']>=1),1]=1
    # A1[np.where(test_data['EMPLOYEE_GROUP']>=1),0]=0

    out = open('/Users/jithon/Desktop/mindef/out.csv', 'w')
    for row in A1:
        out.write('%.12f' % row[1])
        out.write('\n')
    
    out.close()

else:
# cross validation
#clf = RandomForestClassifier(n_estimators=1000,n_jobs=-1)
## specify parameters and distributions to sample from
#param_dist = {"max_features": sp_randint(4, 25),
#              "criterion": ["gini", "entropy"]}
#
## run randomized search
#n_iter_search =50
#log_loss1 = make_scorer(log_loss, eps=0.001)
#random_search = RandomizedSearchCV(clf, param_distributions=param_dist,
#                                   n_iter=n_iter_search,scoring=log_loss1)
#
#start = time()
#random_search.fit(train[features], train['RESIGNED'])
#print("RandomizedSearchCV took %.2f seconds for %d candidates"
#      " parameter settings." % ((time() - start), n_iter_search))
#report(random_search.grid_scores_)
#
    param_grid = {'max_features': np.arange(5,51),
                'bootstrap':[True, False]}
    clf = RandomForestClassifier(class_weight='auto',n_jobs=-1,n_estimators=2000)
    # run grid search
    log_loss1 = make_scorer(log_loss, eps=0.001, greater_is_better=False)
    grid_search = GridSearchCV(clf, param_grid=param_grid,scoring=log_loss1,cv=3)
    start = time()
    grid_search.fit(train_data[features], train_data['RESIGNED'])

    print("GridSearchCV took %.2f seconds for %d candidate parameter settings."
      % (time() - start, len(grid_search.grid_scores_)))
    report(grid_search.grid_scores_)

    CEL = log_loss_score(A1,test_data)
       
    plt.plot(np.arange(1,np.array(features).shape[0]+1),np.sort(rf.feature_importances_))
    sort_index = np.argsort(rf.feature_importances_)
    labels = np.array(features)[sort_index]
    plt.xticks(np.arange(1,np.array(features).shape[0]+1), labels, rotation='vertical')

    [H,xedges,yedges]=np.histogram2d(A.flatten(),test_data['RESIGNED'],bins=2)

    print(H)
    print(CEL)

#print("Train a XGBoost modloel")
#params = {"objective": "binary:logistic",
#          "eta": 0.3,
#          "max_depth": 5,
#          "min_child_weight": 3,
#          "silent": 1,
#          "subsample": 0.7,
#          "colsample_bytree": 0.7,
#          "seed": 1}
# num_trees=250
# gbm = xgb.train(params, xgb.DMatrix(train[features], train["signal"]), num_trees)

# print("Make predictions on the test set")
# test_probs = (rf.predict_proba(test[features])[:,1] +
              # gbm.predict(xgb.DMatrix(test[features])))/2
# submission = pd.DataFrame({"id": test["id"], "prediction": test_probs})
# submission.to_csv("rf_xgboost_submission.csv", index=False)