# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 23:21:27 2015

@author: jithon
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 21:06:03 2015

@author: jithon
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
import scipy

mat_dict = scipy.io.loadmat('/Users/jithon/Desktop/common/data.mat')

test_Y = mat_dict['test_labels']
test_X = mat_dict['test_images'].T
train_Y = mat_dict['train_labels']
train_X = mat_dict['train_images'].T

mean_X = np.mean(train_X,axis=0)
std_X = np.std(train_X,axis=0)
nonzero_index = std_X!=0
test_X = (test_X[:,nonzero_index]-mean_X[nonzero_index])/std_X[nonzero_index]
train_X = (train_X[:,nonzero_index]-mean_X[nonzero_index])/std_X[nonzero_index]

test_X = np.append(np.ones((test_X.shape[0],1)),test_X,axis=1)
train_X = np.append(np.ones((train_X.shape[0],1)),train_X,axis=1)

# Create linear regression object
svmreg = svm.SVC(verbose=True,max_iter=1000,kernel='linear')

# Train the model using the training sets
svmreg.fit(train_X, train_Y)

# The mean square error
#print("Residual sum of squares: %.2f"
#      % np.mean((regr.predict(test_X) - test_Y) ** 2))
## Explained variance score: 1 is perfect prediction
#print('Variance score: %.2f' % regr.score(test_X, test_Y))

predicted_Y = svmreg.predict(test_X)
test_Y = np.ravel(test_Y)

plt.hist2d(test_Y,predicted_Y)

diff_results = predicted_Y-test_Y
index=np.where(diff_results==0)
accuracy = index[0].shape[0]/predicted_Y.shape[0]
print('Accuracy: %.2f' % accuracy)

plt.show()
