# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 21:06:03 2015

@author: jithon
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model

data = np.loadtxt('housing.data')
data = np.append(np.ones((data.shape[0],1)),data,axis=1)

train_X = data[0:400,0:-1]
train_Y = data[0:400,-1]
test_X = data[401:505,0:-1]
test_Y = data[401:505,-1]

# Create linear regression object
regr = linear_model.LinearRegression()

# Train the model using the training sets
regr.fit(train_X, train_Y)

# The coefficients
print('Coefficients: \n', regr.coef_)
# The mean square error
print("Residual sum of squares: %.2f"
      % np.mean((regr.predict(test_X) - test_Y) ** 2))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % regr.score(test_X, test_Y))

predicted_Y = regr.predict(test_X)
sorted_test_Y = np.sort(test_Y)
sorted_index = np.argsort(test_Y)
# Plot outputs
plt.scatter(np.arange(1,test_Y.shape[0]+1), predicted_Y[sorted_index], color='red')
plt.scatter(np.arange(1,test_Y.shape[0]+1), sorted_test_Y, color='blue')
plt.xlabel('test_Y')
plt.ylabel('predicted_Y')
plt.show()
